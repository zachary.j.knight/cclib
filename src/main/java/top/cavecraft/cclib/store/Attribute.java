package top.cavecraft.cclib.store;

public enum Attribute {
    HISTORIC(0),
    STATISTICAL(1),
    SHAMEFUL(2),
    FRAGILE(3),
    ROBUST(4),
    UNIQUE(5);

    private final int index;

    Attribute(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public Attribute getOpposite() {
        switch (this) {
            case STATISTICAL:
                return SHAMEFUL;
            case SHAMEFUL:
                return STATISTICAL;
            case ROBUST:
                return FRAGILE;
            case FRAGILE:
                return ROBUST;
        }
        return null;
    }
}
