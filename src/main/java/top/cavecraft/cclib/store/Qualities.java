package top.cavecraft.cclib.store;

public enum Qualities {
    NEW,
    FRESH,
    SHINY,
    DUSTY,
    TARNISHED
}
