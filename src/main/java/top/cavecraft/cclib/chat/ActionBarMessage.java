package top.cavecraft.cclib.chat;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import top.cavecraft.cclib.ICCLib.IActionBarMessage;

public class ActionBarMessage implements IActionBarMessage {
    PacketPlayOutChat packet;

    public ActionBarMessage(String message){
        IChatBaseComponent serialized = IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + message + "\"}");
        packet = new PacketPlayOutChat(serialized, (byte) 2);
    }

    public void sendTo(Player player){
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
    }
}
