package top.cavecraft.cclib;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import top.cavecraft.cclib.chat.ActionBarMessage;
import top.cavecraft.cclib.game.GameUtils;
import top.cavecraft.cclib.game.GemAmountExtractor;
import top.cavecraft.cclib.game.TickReq;
import top.cavecraft.cclib.scoreboard.ChildScoreboard;
import top.cavecraft.cclib.scoreboard.ScoreboardManager;
import top.cavecraft.cclib.store.Attribute;
import top.cavecraft.cclib.store.Qualities;

import java.awt.*;
import java.util.*;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public final class CCLib extends JavaPlugin implements ICCLib, Listener {
    public static CCLib plugin;

    private GameUtils gameUtils = null;
    private ScoreboardManager scoreboardManager;
    public static final String CONNECTION_STRING = "mongodb://localhost:27017";
    public static String uuid = "";
    public Random random;

    private final MongoClient mongoClient;
    private final MongoCollection<Document> gems;
    private final MongoCollection<Document> servers;
    private final MongoCollection<Document> plugins;
    private final MongoCollection<Document> archetypes;
    private final MongoCollection<Document> wantedServers;
    private final MongoCollection<Document> purchases;
    private final MongoCollection<Document> products;

    public CCLib() {
        random = new Random();
        ConnectionString connectionString = new ConnectionString(CONNECTION_STRING);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .retryWrites(true)
                .build();
        mongoClient = MongoClients.create(mongoClientSettings);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
        gems = mongoDatabase.getCollection("gems");
        servers = mongoDatabase.getCollection("servers");
        plugins = mongoDatabase.getCollection("plugins");
        archetypes = mongoDatabase.getCollection("archetypes");
        wantedServers = mongoDatabase.getCollection("wantedServers");
        purchases = mongoDatabase.getCollection("purchases"); //item uuid: random uuid, player uuid: owner's uuid
        products = mongoDatabase.getCollection("products");
    }

    @Override
    public void onEnable() {
        plugin = this;

        if (getConfig().getString("uuid") == null) {
            getConfig().addDefault("uuid", UUID.randomUUID().toString());
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
            saveConfig();
        }

        uuid = getConfig().getString("uuid");

        Document serverDocument = servers.find(eq("uuid", uuid)).first();

        if (serverDocument == null || serverDocument.get("available") == null) {
            servers.deleteMany(eq("uuid", uuid));
            serverDocument = new Document("_id", new ObjectId());
            serverDocument
                    .append("uuid", uuid)
                    .append("address", getServer().getIp())
                    .append("port", getServer().getPort())
                    .append("gameType", getConfig().getString("gametype"))
                    .append("players", 0)
                    .append("available", true);
            servers.insertOne(serverDocument);
        } else {
            servers.updateOne(
                    eq("uuid", uuid),
                    combine(
                            set("gameType", getConfig().getString("gametype")),
                            set("address", getServer().getIp()),
                            set("port", getServer().getPort()),
                            set("available", true),
                            set("players", 0)
                    )
            );
        }

        scoreboardManager = new ScoreboardManager();
        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(scoreboardManager, this);

    }

    public void addProduct(int rarity, int price, String minecraftItem, String name, String description) {
        String uuid = UUID.randomUUID().toString();
        Document productDocument = products.find(eq("name", name)).first();
        if (productDocument == null) {
            products.deleteMany(eq("uuid", uuid));
            productDocument = new Document("_id", new ObjectId());
            productDocument
                    .append("uuid", uuid)
                    .append("rarity", rarity)
                    .append("price", price)
                    .append("minecraftItem", minecraftItem)
                    .append("name", name)
                    .append("description", description);
            products.insertOne(productDocument);
        } else {
            products.updateOne(
                    eq("uuid", uuid),
                    combine(
                            set("rarity", rarity),
                            set("price", price),
                            set("minecraftItem", minecraftItem),
                            set("name", name),
                            set("description", description)
                    )
            );
        }
    }

    public void addPurchase(UUID productUuid, UUID playerUuid, List<Document> attributeProperties) {
        String uuid = UUID.randomUUID().toString();
        Document purchaseDocument = purchases.find(eq("uuid", uuid)).first();
        if (purchaseDocument == null) {
            purchases.deleteMany(eq("uuid", uuid));
            purchaseDocument = new Document("_id", new ObjectId());
            purchaseDocument
                    .append("uuid", uuid)
                    .append("productUuid", productUuid)
                    .append("playerUuid", playerUuid)
                    .append("quality", getRandomQuality()) //
                    .append("attributes", createAttributes()) // max in this list is 2. not more than one of each kind.
                    .append("attributeProperties", attributeProperties);
            purchases.insertOne(purchaseDocument);
        } else {
            purchases.updateOne(
                    eq("uuid", uuid),
                    combine(
                            set("productUuid", productUuid),
                            set("playerUuid", playerUuid),
                            set("quality", getRandomQuality()),
                            set("attributes", createAttributes()),
                            set("attributeProperties", attributeProperties)
                    )
            );
        }
    }

    public List<Integer> createAttributes() {
        List<Attribute> attributes = Arrays.asList(
                Attribute.HISTORIC,
                Attribute.STATISTICAL,
                Attribute.SHAMEFUL,
                Attribute.FRAGILE,
                Attribute.ROBUST,
                Attribute.UNIQUE
        );
        Collections.shuffle(attributes);
        List<Integer> result = new ArrayList<>();

        if (Math.random() > 0.7) {
            int attributesLength = (int) Math.floor(Math.random() * 2) + 1;

            if (attributesLength > 0) {
                for (int i = 0; i < attributesLength; i++) {
                    result.add(attributes.get(0).getIndex());
                    if (attributes.get(0).getOpposite() != null) {
                        attributes.remove(attributes.get(0).getOpposite());
                    }
                    attributes.remove(0);
                }
            }
        }

        return result;
    }

    private Qualities getRandomQuality() {
        List<Qualities> VALUES =
                Collections.unmodifiableList(Arrays.asList(Qualities.values()));
        int SIZE = VALUES.size();

        return VALUES.get(random.nextInt(SIZE));
    }

    @Override
    public void onDisable() {
        setAvailable(false);
        mongoClient.close();
    }

    @Override
    public IGameUtils enableGameFeatures(List<ITickReq> tickRequirements) {
        gameUtils = new GameUtils(tickRequirements);
        return gameUtils;
    }

    @Override
    public void enableGameFeatures(List<ITickReq> tickRequirements, Runnable startGame, Runnable teleport) {
        gameUtils = new GameUtils(tickRequirements, startGame, teleport);
        getServer().getPluginManager().registerEvents(gameUtils, this);
    }

    @Override
    public IGameUtils getGameUtils() {
        return gameUtils;
    }

    @Override
    public ITickReq createTickReq(int numberOfPlayers, int seconds, boolean canStart) {
        return new TickReq(numberOfPlayers, seconds, canStart);
    }

    @Override
    public ItemStack createNamedItem(Material material, int amount, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        item.setItemMeta(itemMeta);
        return item;
    }

    @Override
    public void restartServer() {
        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
        for (Player p : players) {
            p.kickPlayer("Server is restarting!");
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            List<String> command = new ArrayList<>();
            command.add("./restart.sh");
            try {
                new ProcessBuilder(command).start();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }));

        Bukkit.shutdown();
    }

    @Override
    public IScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

    @Override
    public void awardGems(Player player, IGameUtils.GemAmount amount) {
        try {
            double total;
            int intAmount = (int) (GemAmountExtractor.getAmount(amount) * 10.0);

            Document playerDocument = gems.find(eq("uuid", player.getUniqueId().toString())).first();

            if (playerDocument == null || playerDocument.get("cgems") == null) {
                playerDocument = new Document("_id", new ObjectId());
                playerDocument
                        .append("uuid", player.getUniqueId().toString())
                        .append("cgems", intAmount);
                gems.insertOne(playerDocument);
            }

            try {
                total = playerDocument.getInteger("cgems") + intAmount;
            } catch (ClassCastException e) {
                //This converts old playerdata to integers.
                total = Math.round(playerDocument.getDouble("cgems")) + intAmount;
            }

            gems.updateOne(
                    eq("uuid", player.getUniqueId().toString()),
                    set("cgems", total)
            );

            player.sendMessage(
                    ChatColor.DARK_PURPLE +
                            "You earned " +
                            ChatColor.GOLD +
                            (intAmount / 10.0) +
                            ChatColor.DARK_PURPLE +
                            " CaveGems. Total: " +
                            ChatColor.GOLD +
                            (total / 10.0)
            );
            player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 2);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + "Error occured while awarding gems.");
            e.printStackTrace();
        }
    }

    @Override
    public double getGems(Player player) {
        try {
            double total;

            Document playerDocument = gems.find(eq("uuid", player.getUniqueId().toString())).first();

            if (playerDocument == null || playerDocument.get("cgems") == null) {
                playerDocument = new Document("_id", new ObjectId());
                playerDocument
                        .append("uuid", player.getUniqueId().toString())
                        .append("cgems", 0);
                gems.insertOne(playerDocument);
            }

            try {
                total = playerDocument.getInteger("cgems");
            } catch (ClassCastException e) {
                //This converts old playerdata to integers.
                total = Math.round(playerDocument.getDouble("cgems"));
            }

            return total / 10.0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void decreaseGems(Player player, double decrease) {
        try {
            double total;
            int intAmount = (int) Math.round(decrease * -10.0);

            Document playerDocument = gems.find(eq("uuid", player.getUniqueId().toString())).first();

            if (playerDocument == null || playerDocument.get("cgems") == null) {
                playerDocument = new Document("_id", new ObjectId());
                playerDocument
                        .append("uuid", player.getUniqueId().toString())
                        .append("cgems", intAmount);
                gems.insertOne(playerDocument);
            }

            try {
                total = playerDocument.getInteger("cgems") + intAmount;
            } catch (ClassCastException e) {
                //This converts old playerdata to integers.
                total = Math.round(playerDocument.getDouble("cgems")) + intAmount;
            }

            gems.updateOne(
                    eq("uuid", player.getUniqueId().toString()),
                    set("cgems", total)
            );

            player.sendMessage(
                    ChatColor.DARK_PURPLE +
                            "You earned " +
                            ChatColor.GOLD +
                            (intAmount / 10.0) +
                            ChatColor.DARK_PURPLE +
                            " CaveGems. Total: " +
                            ChatColor.GOLD +
                            (total / 10.0)
            );
            player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 2);
        } catch (Exception e) {
            player.sendMessage(ChatColor.RED + "Error occured while awarding gems.");
            e.printStackTrace();
        }
    }

    public void setAvailable(boolean available) {
        servers.updateOne(
                eq("uuid", uuid),
                set("available", available)
        );
    }

    public void setPlayers(int players) {
        servers.updateOne(
                eq("uuid", uuid),
                set("players", players)
        );
    }

    @Override
    public IChildScoreboard createChildScoreboard() {
        return new ChildScoreboard();
    }

    @Override
    public Location offset(Location original, double xoff, double yoff, double zoff) {
        return new Location(original.getWorld(), original.getX() + xoff, original.getY() + yoff, original.getZ() + zoff);
    }

    @Override
    public IActionBarMessage createActionBarMessage(String message) {
        return new ActionBarMessage(message);
    }

    @EventHandler
    public void achievement(PlayerAchievementAwardedEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void connect(PlayerJoinEvent event) {
        if (getServer().getOnlinePlayers().size() >= getServer().getMaxPlayers() && gameUtils == null) {
            setAvailable(false);
        }
        setPlayers(getServer().getOnlinePlayers().size());
    }

    @EventHandler
    public void leave(PlayerQuitEvent event) {
        if (getServer().getOnlinePlayers().size() < getServer().getMaxPlayers() && gameUtils == null) {
            setAvailable(true);
        }
        setPlayers(getServer().getOnlinePlayers().size());
    }

    @EventHandler
    public void kick(PlayerKickEvent event) {
        if (getServer().getOnlinePlayers().size() < getServer().getMaxPlayers() && gameUtils == null) {
            setAvailable(true);
        }
        setPlayers(getServer().getOnlinePlayers().size());
    }
}
